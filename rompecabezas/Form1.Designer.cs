﻿namespace rompecabezas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonImageBrowse = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupboxPuzzle = new System.Windows.Forms.Panel();
            this.groupBoxPreview = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progresoTiempo = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonImageBrowse
            // 
            this.buttonImageBrowse.Location = new System.Drawing.Point(34, 22);
            this.buttonImageBrowse.Name = "buttonImageBrowse";
            this.buttonImageBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonImageBrowse.TabIndex = 1;
            this.buttonImageBrowse.Text = "Imagen";
            this.buttonImageBrowse.UseVisualStyleBackColor = true;
            this.buttonImageBrowse.Click += new System.EventHandler(this.btn_click_subir_imagen);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(115, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Iniciar";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupboxPuzzle
            // 
            this.groupboxPuzzle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupboxPuzzle.Location = new System.Drawing.Point(34, 111);
            this.groupboxPuzzle.Name = "groupboxPuzzle";
            this.groupboxPuzzle.Size = new System.Drawing.Size(372, 199);
            this.groupboxPuzzle.TabIndex = 4;
            // 
            // groupBoxPreview
            // 
            this.groupBoxPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBoxPreview.Location = new System.Drawing.Point(416, 199);
            this.groupBoxPreview.Name = "groupBoxPreview";
            this.groupBoxPreview.Size = new System.Drawing.Size(200, 111);
            this.groupBoxPreview.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Rompecabezas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(413, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Vista previa";
            // 
            // progresoTiempo
            // 
            this.progresoTiempo.Location = new System.Drawing.Point(34, 316);
            this.progresoTiempo.Maximum = 300;
            this.progresoTiempo.Name = "progresoTiempo";
            this.progresoTiempo.Size = new System.Drawing.Size(673, 23);
            this.progresoTiempo.Step = 1;
            this.progresoTiempo.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(427, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 50);
            this.label3.TabIndex = 8;
            this.label3.Text = "Lo setimos has fallado \r\nvuelve intenta con otro";
            this.label3.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 348);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progresoTiempo);
            this.Controls.Add(this.groupBoxPreview);
            this.Controls.Add(this.groupboxPuzzle);
            this.Controls.Add(this.buttonImageBrowse);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonImageBrowse;
        private System.Windows.Forms.Button btnStart;

        private System.Windows.Forms.Panel groupboxPuzzle;
        private System.Windows.Forms.Panel groupBoxPreview;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progresoTiempo;
        private System.Windows.Forms.Label label3;
    }
}

