﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rompecabezas
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }
        OpenFileDialog openFileDialog = null;
        Image image;
        PictureBox picBoxWhole = null;

        private void btn_click_subir_imagen(object sender, EventArgs e)
        {
            if(this.openFileDialog == null)
            {
                this.openFileDialog = new OpenFileDialog();
            }
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                image = this.createBitmapImage(Image.FromFile(openFileDialog.FileName),groupboxPuzzle);
                Image imagepreview = this.createBitmapImage(Image.FromFile(openFileDialog.FileName), groupBoxPreview);
                if (picBoxWhole == null)
                {
                    picBoxWhole = new PictureBox();
                    picBoxWhole.Width = groupBoxPreview.Width;
                    picBoxWhole.Height = groupBoxPreview.Height;
                    groupBoxPreview.Controls.Add(picBoxWhole);

                }

                picBoxWhole.Image = imagepreview;
            }
            
        }
        private Bitmap createBitmapImage(Image imagen,Panel contenedorx)
        {
            Bitmap objetoImagen = new Bitmap(contenedorx.Width, contenedorx.Height);
            Graphics objetoGrafico = Graphics.FromImage(objetoImagen);
            objetoGrafico.Clear(Color.White);
            objetoGrafico.DrawImage(imagen, new Rectangle(0, 0, contenedorx.Width, contenedorx.Height));
            objetoGrafico.Flush();

            return objetoImagen;

        }
        PictureBox[] picBoxes = null;
        Image[] images = null;
        const int LEVEL_1_NUM = 16;
        Timer intervalo = null;
        private int cont = 1;
        private void btnStart_Click(object sender, EventArgs e)
        {

            if (this.image == null)
            {
                MessageBox.Show("Prueba cargando una imagen");
                return;
            }

            this.restartGame();
            progresoTiempo.Value = 0;
            /*if (this.picBoxWhole != null)
            {
                groupboxPuzzle.Controls.Remove(this.picBoxWhole);
                picBoxWhole.Dispose();
                picBoxWhole = null;
            }*/

            cont = 1;

            intervalo = new Timer();
            intervalo.Interval = 3000;
            intervalo.Tick += new EventHandler(tick_intervalo);
            intervalo.Enabled = true;

            if (this.picBoxes == null)
            {
                this.picBoxes = new PictureBox[LEVEL_1_NUM];
                this.images = new Image[LEVEL_1_NUM];
            }
            

            int numRow = (int)Math.Sqrt(LEVEL_1_NUM);
            int numCol = numRow;
            int unitX = groupboxPuzzle.Width / numRow;
            int unitY = groupboxPuzzle.Height / numCol;
            int[] indice = new int[LEVEL_1_NUM];

            for(int i = 0; i< LEVEL_1_NUM; i++)
            {
                indice[i] = i;
                if(picBoxes[i] == null)
                {
                    picBoxes[i] = new MyPictureBox();
                    picBoxes[i].Click += new EventHandler(this.piezaClick);

                    picBoxes[i].BorderStyle = BorderStyle.Fixed3D;
                }
                picBoxes[i].Width = unitX;
                picBoxes[i].Height = unitY;

                ((MyPictureBox)picBoxes[i]).Index = i;

                createBitmapImage(this.image, this.images, i, numRow, numCol, unitX, unitY);
                picBoxes[i].Location = new Point(unitX * (i % numCol), unitY * (i / numCol));
                if (!groupboxPuzzle.Controls.Contains(this.picBoxes[i]))
                {
                    groupboxPuzzle.Controls.Add(this.picBoxes[i]);
                }
            }

            shuffle(ref indice);
            for(int i = 0; i< LEVEL_1_NUM; i++)
            {
                picBoxes[i].Image = images[indice[i]];
                ((MyPictureBox)picBoxes[i]).ImageIndex = indice[i];
            }

        }
        MyPictureBox primeraPieza = null;
        MyPictureBox segundaPieza = null;
        public void piezaClick(object sender, EventArgs e)
        {
            ((MyPictureBox)sender).BorderStyle = BorderStyle.FixedSingle;

            if(primeraPieza == null)
            {
                primeraPieza = (MyPictureBox)sender;
                primeraPieza.BorderStyle = BorderStyle.FixedSingle;
            }
            else if (segundaPieza == null)
            {
                segundaPieza = (MyPictureBox)sender;
                primeraPieza.BorderStyle = BorderStyle.Fixed3D;
                primeraPieza.BorderStyle = BorderStyle.FixedSingle;

                SwitchImages(primeraPieza, segundaPieza);
                primeraPieza = null;
                segundaPieza = null;
            }
            /*else
            {
                firstBox = secondBox;
                firstBox.BorderStyle = BorderStyle.Fixed3D;
                secondBox = (MyPictureBox)sender;
                secondBox.BorderStyle = BorderStyle.FixedSingle;

                this.SwitchImages(firstBox, secondBox);
            }*/

        }

        public void createBitmapImage(Image image, Image [] images, int index, int numRow, int numCol,int unitX,int unitY)
        {
            images[index] = new Bitmap(unitX, unitY);
            Graphics objetoGraphics = Graphics.FromImage(images[index]);
            objetoGraphics.Clear(Color.White);

            objetoGraphics.DrawImage(image, 
                new Rectangle(0,0,unitX,unitY),
                new Rectangle(unitX * (index % numCol), unitY * (index / numCol), unitX, unitY),GraphicsUnit.Pixel);
            objetoGraphics.Flush();
        }
        private void shuffle(ref int [] array)
        {
            Random rng = new Random();
            int n = array.Length;

            while(n> 1)
            {
                int k = rng.Next(n);
                n--;
                int temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }

        private void SwitchImages(MyPictureBox box1, MyPictureBox box2)
        {
            int tmp = box2.ImageIndex;
            box2.Image = images[box1.ImageIndex];
            box2.ImageIndex = box1.ImageIndex;

            box1.Image = images[tmp];
            box1.ImageIndex = tmp;
            
            if (isListo())
            {
                Console.Write("hola amigos");
                intervalo.Stop();
                this.restartGame();
                MessageBox.Show("has ganado aprovecha tu victoria");
                return;
            }

        }
        private bool isListo()
        {
            for(int i = 0; i < LEVEL_1_NUM; i++)
            {
                if(((MyPictureBox)picBoxes[i]).ImageIndex != ((MyPictureBox)picBoxes[i]).Index)
                    return false;
                
            }
            return true;
        }
        private void tick_intervalo(object Sender, EventArgs e)
        {
            progresoTiempo.Increment(1);

            cont += 1;
            if (cont >= 300)
            {
                
                label3.Visible = true;
                intervalo.Stop();
                this.restartGame();

                MessageBox.Show("ALERTA", "lo setimos has perdido", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                
                
                
            }

            
        }
        public void restartGame()
        {
            
            intervalo = null;
            cont = 0;

        }
    }
    
}
